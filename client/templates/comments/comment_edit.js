Template.commentEdit.onCreated(function(){
	Session.set('commentEditErrors',{});
});

Template.commentEdit.helpers({
	thoughttyper: function(){
		var comment = Comments.findOne(Session.get('editCommentId'));
		var post = Posts.findOne(comment.postId);
		return post.thoughtTypes;
	},
	contenter: function(){
		var comment = Comments.findOne(Session.get('editCommentId'));
		return comment.body;
	},
	errorMessage: function (field){
		return Session.get('commentEditErrors')[field];
	},
	errorClass: function (field){
		return !!Session.get('commentEditErrors')[field] ? 'has-error':'';
	},
	
	

});

Template.commentEdit.events({
	'submit form': function(e,template){
		e.preventDefault();

		var commentId = Session.get('editCommentId');
		var body = $(e.target).find('#summernote').code();
		var tte = $(e.target).find('[name=thoughtType]').val();
		var commentProperties = {
			body: $(e.target).find('#summernote').code(),
			thoughtType: $(e.target).find('[name=thoughtType]').val(),
			
		};

		var errors ={};
		if (! body){
			errors.body = "Please write in your thoughts "
			return Session.set('commentEditErrors',errors);

		}

		Comments.update(commentId, {$set: commentProperties}, function(error){
			if (error){
				throwError(error.reason);
			}else {
				Session.set('editCommentId',false);
			}
		})
	},
	'click .remove':function(e){
		e.preventDefault();
		if (confirm("Delete this thought")){
			var commentId = Session.get('editCommentId');
			Comments.remove(commentId);
			
		}
	},
	'click .cancel':function(e){
		e.preventDefault();
		Session.set('editCommentId',false);
	}

});


Template.commentEdit.rendered = function(){
	$('#summernote').summernote({
		height: 100,
		
	});
}