Template.commentItem.helpers({
	submittedText: function(){
		return this.submitted.toString();
	},
	ownComment: function(){
		return (this.userId === Meteor.userId() || Roles.userIsInRole(Meteor.userId(),'admin'));
	},
	isEditingComment: function(){
		return Session.get('editCommentId') === this._id;
	},
	isCommentReplying: function(){
		return Session.get('replyCommentId') === this._id;
	},
	convers: function(){
		return this.headId;
	},
	discuss: function(){
		var rout = Router.current().route.getName() ;
		
		return  !(rout === 'postPageConv');
	
	}


	
});

Template.commentItem.events({
	"click a.edit": function(e){
		Session.set('editCommentId',this._id);
	} ,
	"click a.reply": function(e){
		Session.set('replyCommentId',this._id);
	}
})