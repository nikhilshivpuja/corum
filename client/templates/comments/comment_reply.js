Template.commentReply.onCreated(function(){
	Session.set('commentReplyErrors',{});
});
Template.commentReply.helpers({
	thoughttyper: function(){
		var comment = Comments.findOne(Session.get('replyCommentId'));
		var post = Posts.findOne(comment.postId);
		return post.thoughtTypes;
	},
	
	errorMessage: function (field){
		return Session.get('commentReplyErrors')[field];
	},
	errorClass: function (field){
		return !!Session.get('commentReplyErrors')[field] ? 'has-error':'';
	},
	
	

});

Template.commentReply.events({
	'submit form': function(e,template){
		e.preventDefault();
		var head;
		var hc = Comments.findOne(Session.get('replyCommentId'));
		var $body = $(e.target).find('#summernote');
		if (! hc.headId){
			head = Session.get('replyCommentId');

		}
		else{
			head = hc.headId;
		}
		var comment = {
			body: $body.code(),
			thoughtType: $(e.target).find('[name=thoughtType]').val(),
			postId: hc.postId,
			
			headId: head
		};

		var errors = {};
		if (! comment.body){
			errors.body = "Please write in your thoughts "
			return Session.set('commentSubmitErrors',errors);

		}

		Meteor.call('commentInsert',comment, function(error, commentId){
			if (error){
				throwError(error.reason);
			}else{
				$body.code('');
				Session.set('replyCommentId',false);
			}
		});
	}
})

Template.commentReply.rendered = function(){
	$('#summernote').summernote({
		height: 100,
		
	});
}