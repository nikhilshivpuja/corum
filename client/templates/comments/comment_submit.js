Template.commentSubmit.onCreated(function(){
	Session.set('commentSubmitErrors',{});
});

Template.commentSubmit.helpers({
	errorMessage: function (field){
		return Session.get('commentSubmitErrors')[field];
	},
	errorClass: function (field){
		return !!Session.get('commentSubmitErrors')[field] ? 'has-error':'';
	},
	thoughttype: function(){
		var post = Posts.findOne(template.data._id);
		return post.thoughtTypes
	}
});

Template.commentSubmit.events({
	'submit form': function(e,template){
		e.preventDefault();
		var $body = $(e.target).find('#summernote');
		var comment = {
			body: $body.code(),
			thoughtType: $(e.target).find('[name=thoughtType]').val(),
			postId: template.data._id
		};

		var errors ={};
		if (! comment.body){
			errors.body = "Please write in your thoughts "
			return Session.set('commentSubmitErrors',errors);

		}

		Meteor.call('commentInsert',comment, function(error, commentId){
			if (error){
				throwError(error.reason);
			}else{
				$body.code('');
			}
		});
	}
});


Template.commentSubmit.rendered = function(){
	$('#summernote').summernote({
		height: 200,
		
	});
}