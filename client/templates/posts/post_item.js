Template.postItem.helpers({
	ownPost: function(){
		
		return this.userId === Meteor.userId() || Roles.userIsInRole(Meteor.userId(), 'admin');
	},
	domain: function(){
		var a= document.createElement('a');
		
		return this.url.substring(0,150);
	},
	upvotedClass: function(){
		var userId = Meteor.userId();
		if (userId && !_.include(this.upvoters, userId)){
			return 'btn-primary upvotable';
		} else {
			return 'disabled';
		}
	},
	postUrl: function(){
		return this.shortUrl ? this.shortUrl : this.url;
	},
	category: function(){
		var cat = Types.findOne({_id: this.typeId});
		return cat.title;
	}
	
});

Template.postItem.events({
	'click .upvotable': function(e){
		e.preventDefault();
		Meteor.call('upvote', this._id);
	}
})