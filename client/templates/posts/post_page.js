
Template.postPage.helpers({
	comments: function(){
		Session.set('postId',this._id);

		return Comments.find({postId: this._id});

	},
	content: function(){
		return this.url;
	},
	idea: function(){
		
		return this._id;

	},
	router:function(){
		return Router.current().route.getName() ;
	},
	postroot: function(){
		var rout = Router.current().route.getName() ;
		return ( rout === 'postPage');
	},
	thoughtTyper: function(){
		var tts = this.thoughtTypes;
		
		var arrayed = [];
		var i;
		for (i = 0; i < tts.length; i++) { 
			var tip ={};
			tip.names = tts[i];
    		tip.post = this._id;
    		tip.count = Comments.find({postId: this._id , thoughtType: tts[i]}).count();
    		arrayed.push(tip);

		}			
		return arrayed;
	},
	

});

Template.postPage.events({
	'click .comad':function(e,template){
		e.preventDefault();
		$('html,body').animate({
			scrollTop: $("#comad").offset().top
		},600);
	}
});

Template.postPage.rendered = function(){
	var rout = Router.current().route.getName() ;
	if (rout === 'postPage')
	{Meteor.call('upview', Session.get('postId'));}
	

}






