Template.postSubmit.onCreated(function(){
	Session.set('postSubmitErrors',{});
	
});

Template.postSubmit.rendered = function(){
	$('#summernote').summernote({
		height: 200,
		focus: true
	});
}

Template.postSubmit.helpers({
	errorMessage: function(field) {
		return Session.get('postSubmitErrors')[field];
	},
	errorClass: function(field) {
		return !!Session.get('postSubmitErrors')[field] ? 'has-error' : '';
	},
	category: function(){
	var user = Meteor.user();
			var arrayed = user.profile.restrict.split(',');
	return Types.find( { $or: [{ restrictive: { $ne: true}} , { title: {$in: arrayed}}]});

	}
});


Template.postSubmit.events({
	'submit form':function(e){
		e.preventDefault();

		var post={
			url: $(e.target).find('#summernote').code(),
			title: $(e.target).find('[name=title]').val(),
			tt: $(e.target).find('[name=tt]').val(),
			typeId: $(e.target).find('[name=category]').val()
		};

		var errors = validatePost(post);
		if (errors.title || errors.url || errors.tt )
			return Session.set('postSubmitErrors',errors);

		Meteor.call('postInsert',post,function(error, result){
			
			if (error)
				return throwError(error.reason);

			if (result.postExists)
				throwError("this link has already been posted");

			Router.go('postPage',{_id:result._id});

		});

	
	}
});

