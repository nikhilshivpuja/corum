Template.adminTypes.helpers({
	types: function(){
		return Types.find();
	}
});




Template.typeItem.events({
	"click button.delete": function(e){
		e.preventDefault();
		if (confirm("Delete this type")){
			Meteor.call('typeDelete',this._id);
		}
	},
});

Template.typeMake.helpers({
	errorMessage: function (field){
		return Session.get('typeSubmitErrors')[field];
	},
	errorClass: function (field){
		return !!Session.get('typeSubmitErrors')[field] ? 'has-error':'';
	},
	
});

Template.typeMake.events({
	'submit form': function(e,template){
		e.preventDefault();
		var valid = ($(e.target).find('[name=restrictive]').val() === 'true');
		var type = {
			
			title: $(e.target).find('[name=title]').val(),
			restrictive: valid,
		};

		var errors ={};
		if (!type.title ){
			errors.title = "Please enter title ";
			return Session.set('typeSubmitErrors',errors);

		};

		Meteor.call('typeInsert',type, function(error, typeId){
			if (error){
				throwError(error.reason);
			}
			else{

			}
		});
	}
})