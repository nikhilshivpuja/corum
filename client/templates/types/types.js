Template.types.helpers({
	categorise: function(){
		if (Meteor.user()){
			var user = Meteor.user();
			var arrayed = user.profile.restrict.split(',');
		

		return Types.find( { $or: [{ restrictive: { $ne: true}} , { title: {$in: arrayed}}]});

		}else
		{return Types.find({ restrictive: { $ne: true }});}
	}
});