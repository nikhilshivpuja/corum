Template.userItem.helpers({
	acheck: function(){
		return Roles.userIsInRole(this._id,'admin');
	},
	isEditingLevel: function(){
		return Session.get('editLevelId') === this._id;
	} 

});

Template.userItem.events({
	"click button.admc": function(e){
		Meteor.call('toggleAdmin',this._id);
	},
	"click button.edit": function(e){
		Session.set('editLevelId',this._id);
	},
	"click button.delete": function(e){
		e.preventDefault();
		if (confirm("Delete this user")){
			Meteor.call('deleteUser',this._id);
		}
	},
});

Template.levelEdit.helpers({
	level: function(){
		return Types.find({restrictive: true });
	},
	userLevels: function(){
		var user = Meteor.users.findOne(Session.get('editLevelId'));
		return user.profile.restrict ;
	},

});

Template.levelEdit.events({
	"click button.change": function(e,template){
		e.preventDefault();
		var obj ={
		uid: Session.get('editLevelId'),
		levels: $('[name="levels"]').val(),};
		Meteor.call('levelChange',obj);
		Session.set('editLevelId',false);
		
	}
})