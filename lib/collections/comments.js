Comments =  new Mongo.Collection("comments");

Comments.allow({
	update: function(userId, comment) {return ownsDocument(userId, comment);},
	remove: function(userId, comment) { return ownsDocument(userId, comment);},
});


Meteor.methods({
	commentInsert: function(commentAttributes){
		check(this.userId,String);
		check(commentAttributes,{
			postId: String,
			body: String,
			thoughtType: String,
			headId: Match.Optional(String)
		});
		var user = Meteor.user();
		var post = Posts.findOne(commentAttributes.postId)
		if (!post)
			throw new Meteor.Error('invalid-comment','you must comment on a post');
		comment = _.extend(commentAttributes,{
			userId: user._id,
			author: user.username,
			submitted: new Date()
		});
		if (commentAttributes.headId){
			Comments.update(commentAttributes.headId, {$set: {headId: commentAttributes.headId}});

		}

		Posts.update(comment.postId, {$inc: {commentsCount: 1}});
		Meteor.users.update({_id: user._id}, {$inc: {"profile.commentCount": 1}});

		comment._id = Comments.insert(comment);
		createCommentNotification(comment);
		return comment._id;
	}
});