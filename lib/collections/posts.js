Posts = new Mongo.Collection('posts');

Posts.allow({
	update: function(userId, post) {return ownsDocument(userId, post);},
	remove: function(userId, post) { return ownsDocument(userId, post);},
});

Posts.deny({
	update: function(userId, post, fieldNames){
		return (_.without(fieldNames,'url','title','tt','thoughtTypes','typeId').length>0);
	}
});



Posts.deny({
	update: function(userId,post, fieldNames, modifier){
		var errors = validatePost(modifier.$set);
		return errors.title || errors.url || errors.tt ;
	}
});

Meteor.methods({
	postInsert: function(postAttributes){
		check(Meteor.userId(), String);
		check(postAttributes,{
			title: String,
			url: String,
			tt: String,
			typeId: String
		});

	

	var errors = validatePost(postAttributes);
	if (errors.title || errors.url || errors.tt )
		throw new Meteor.Error('Invalid post',"naughty boy, you must set a title and an url for your post")

	
	var user = Meteor.user();
	var arrays = new Array();
    arrays = postAttributes.tt.split(',');
	var post = _.extend(postAttributes,{
		userId: user._id,
		author: user.username,
		submitted: new Date(),
		commentsCount: 0,
		upvoters: [],
		votes: 0,
		views: 0,
		thoughtTypes: arrays		
	});

	Meteor.users.update({_id: user._id}, {$inc: {"profile.postCount": 1}});
	var postId = Posts.insert(post);
	return {
		_id: postId
	};
},
upvote: function(postId) {
		check(this.userId, String);
		check(postId, String);
		var affected = Posts.update({
			_id: postId,
			upvoters: {$ne: this.userId}
		},{
			$addToSet: {upvoters: this.userId},
			$inc: {votes: 1}
		});
		if (! affected )
			throw new Meteor.Error('invalid', "you weren't able yo make your vote count ");
			
		
	},

upview: function(postId){
	check(postId, String);
	var affected = Posts.update({_id: postId},
		{$inc: {views: 1}
	});
	if (! affected)
		throw new Meteor.Error("invalid","auto updating of view count failing");
}
	
});

validatePost = function (post) {
	var errors = {};
	if (!post.title)
		errors.title = "please Fill in a headline";
	if (!post.url)
		errors.url = "please Fill in a description";
	if (!post.tt)
		errors.tt = "please enter comma separated thought types";
	return errors;
	
}
