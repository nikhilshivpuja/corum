Types = new Mongo.Collection('types');

Meteor.methods({
	typeInsert: function(titleheld){
		check(Meteor.userId(),String);
		check(titleheld,{
			title: String,
			restrictive: Boolean });
		if (Roles.userIsInRole(Meteor.userId(),"admin"))
		{
			var typeId = Types.insert(titleheld);
			return typeId;
		}
		else{
			throw new Meteor.Error("unauthorised",'you must be admin to add a type')
		};
	},
	typeEdit: function(title,typeId){
		check(Meteor.userId(),String);
		check(title, String);
		if (Roles.userIsInRole(Meteor.userId(),"admin"))
		{
			var typeIdd = Types.update(typeId,title);
			return typeIdd;
		}
	},
	typeDelete: function(typeId){
		check(Meteor.userId(),String);
	
		if (Roles.userIsInRole(Meteor.userId(),"admin"))
		{
			Types.remove(typeId);
		}
	},
})