Meteor.methods({
	makeAdmin: function(){
		var currentuser = Meteor.user();
		
		Roles.setUserRoles(currentuser, 'admin');

	},
	toggleAdmin: function(userid){
		var currentuser = Meteor.user();
		if (Roles.userIsInRole(currentuser._id,'admin'))
		{
			if (Roles.userIsInRole(userid,'admin'))
			{
				Roles.removeUsersFromRoles(userid,'admin');
			}else{
				Roles.setUserRoles(userid,'admin');
			};
		};
	},
	levelChange: function(obj){
		var levels = obj.levels;
		var userid = obj.uid;
		var currentuser = Meteor.user();
		if (Roles.userIsInRole(currentuser._id,'admin'))
		{
			Meteor.users.update({_id: userid},{$set: {"profile.restrict": levels}});
		}
		
	},
	deleteUser: function(userid){
		var currentuser = Meteor.user();
		if (Roles.userIsInRole(currentuser._id,'admin'))
		{
			
				Meteor.users.remove(userid)
			
		};

	}
});

