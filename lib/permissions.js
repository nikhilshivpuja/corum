ownsDocument = function(userId, doc){
	return doc && doc.userId === userId || Roles.userIsInRole(this.userId, 'admin');
}