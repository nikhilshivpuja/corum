Router.configure({
	layoutTemplate:'layout',
	loadingTemplate: 'loading',
	notFoundTemplate: 'notFound',
	waitOn: function() {
		return [Meteor.subscribe('notifications'),Meteor.subscribe('types')]
	}
});


Router.route('/posts/:_id',{
	name: 'postPage',


	waitOn: function(){
			

		return [
		 
		Meteor.subscribe('singlePost', this.params._id),
		Meteor.subscribe('comments',this.params._id)];


	},

	data: function() { return Posts.findOne(this.params._id);}
});

Router.route('/posts/conv/:_id/:_convid',{
	name: 'postPageConv',
	template: 'postPage',

	waitOn: function(){
		
		return [
		Meteor.subscribe('singlePost',this.params._id),
		Meteor.subscribe('convers',this.params._convid)];
	},
	data: function() { return Posts.findOne(this.params._id);}

});



Router.route('/posts/types/:_id/:_tt',{
	name: 'postPageTt',
	template: 'postPage',

	waitOn: function(){
		
		return [
		Meteor.subscribe('singlePost',this.params._id),
		Meteor.subscribe('ttas',this.params._tt)];
	},
	data: function() { return Posts.findOne(this.params._id);}

});

Router.route('/posts/:_id/edit',{
	name: 'postEdit',
	waitOn: function(){
		return Meteor.subscribe('singlePost',this.params._id);
	},
	data: function() {return Posts.findOne(this.params._id);}
});

Router.route("/submit",{name: 'postSubmit'});

PostsListController = RouteController.extend({
	template: 'postsList',
	increment: 5,
	postsLimit: function() {
		return parseInt(this.params.postsLimit) || this.increment;
	},
	findOptions: function() {
		return {sort: this.sort , limit: this.postsLimit() };
	},
	subscriptions: function() {
		this.postsSub =  Meteor.subscribe('posts', this.findOptions(),Meteor.userId());
	},
	posts: function() {
		return Posts.find({}, this.findOptions());
	},
	data: function() {
		var hasMore =  this.posts().count()  === this.postsLimit();
		
		return {
			posts: this.posts(),
			ready: this.postsSub.ready,
			nextPath: hasMore ? this.nextPath() : null
		};
	}
});

NewPostsController = PostsListController.extend({
	sort: {submitted: -1, _id: -1},
	nextPath: function(){
		return Router.routes.newPosts.path({postsLimit: this.postsLimit() + this.increment })
	}
});

BestPostsController = PostsListController.extend({
	sort: {votes: -1, submitted: -1, _id: -1},
	nextPath: function(){
		return Router.routes.bestPosts.path({postsLimit: this.postsLimit() + this.increment })
	}
});

ViewedPostsController = PostsListController.extend({
	sort: {views : -1, submitted: -1, _id: -1},
	nextPath: function(){
		return Router.routes.viewedPosts.path({postsLimit: this.postsLimit() + this.increment })
	}
});

Router.route("/category/:categorynum/:postsLimit?",{
	name: 'categoryPosts',
	template: 'postsList',
	
	waitOn: function(){
		
		var limit = parseInt(this.params.postsLimit)||5;
		return Meteor.subscribe('postsCat', this.params.categorynum ,limit);
			
		
	},
	data: function(){
		
			var limit = parseInt(this.params.postsLimit)||5;
		
		return {
			posts: Posts.find({typeId: this.params.categorynum},{sort: {submitted: -1}, limit:limit}),
			nextPath: Router.routes.categoryPosts.path({categorynum: this.params.categorynum, postsLimit: parseInt(this.params.postsLimit) + 5 })}
	
		

},
	
});

Router.route('/',{
	name:'home',
	controller: NewPostsController
});

Router.route('/feed.xml',{
	where: 'server',
	name: 'rss',
	action: function(){
		var feed = new RSS({
			title: "New Micropost Posts",
			description: "The latest dicscussions from corum , the woelds first contextual forum"
		});

		Posts.find({}, {sort: {submitted: -1}, limit: 20}).forEach(function(post){
			feed.item({
				title: post.title,
				description: post.body,
				author: post.author,
				date: post.submitted,
				url: '/posts/'+ post._id
			})

		});
		this.response.write(feed.xml());
		this.response.end();
	}
});

Router.route('/api/posts',{
	where: 'server',
	name: 'apiPosts',
	action: function(){
		var parameters = this.request.query,
			limit = !!parameters.limit ? parseInt(parameters.limit) : 20,
			data = Posts.find({},{limit: limit, fields: {title: 1, author: 1, url: 1, submitted: 1, }}).fetch();
		this.response.write(JSON.stringify(data));
		this.response.end();
	}
});

Router.route('/api/posts/:_id',{
	where: 'server',
	name: 'apiPost',
	action: function(){
		var post = Posts.findOne(this.params._id);
		if(post){
			this.response.write(JSON.stringify(post));	
		} else {
			this.response.writeHead( 404,{'Content-Type': 'text/html'});
			this.response.write(" Discussion not found ");
		}
		this.response.end();
	}
});

Router.route('/adminPage/',{
	name: 'adminPage',
	waitOn: function(){
		var loggedinUser = Meteor.user();
	if ( loggedinUser && Roles.userIsInRole(loggedinUser,'admin') )
		{
		return Meteor.subscribe('users',Meteor.userId());	
		this.next();
	}else{
		
		
		this.render('accessDenied');
	}
		
	},
	
});

Router.route('/adminTypes/',{
	name: 'adminTypes'
	
});



Router.route('/new/:postsLimit?', {name: 'newPosts'});
Router.route('/best/:postsLimit?',{name: 'bestPosts'});
Router.route('/viewed/:postsLimit?',{name: 'viewedPosts'});


var requireLogin = function(){
	if (! Meteor.user()){
		if (Meteor.loggingIn()){
			this.render(this.loadingTemplate);
		}else{
			this.render('accessDenied');
		}
		
	}else
	{
		this.next();
	}
};

var isadmin = function(){
	var loggedinUser = Meteor.user();
	if ( loggedinUser && Roles.userIsInRole(loggedinUser,'admin') )
		{
			
		this.next();
	}else{
		
		
		this.render('accessDenied');
	}
};


if (Meteor.isClient){
Router.onBeforeAction('dataNotFound',{only: 'postPage'});
Router.onBeforeAction(requireLogin,{only: 'postSubmit'});
Router.onBeforeAction(isadmin,{only: 'adminPage'});
Router.onBeforeAction(isadmin,{only: 'adminTypes'});

}



