Bitly = {};

Bitly.shortenURL = function(url){
	if (!Meteor.settings.bitly)
		throw new Meteor.Error(500, 'Please provide a Bitly token in Meteor.settings ');

	var shortenResponse =  Meteor.http.get(
		"https://api-ssl.bitly.com/v3/shorten?",
		{
			timeout: 5000,
			params: {
				"format":"json",
				"access_token": Meteor.settings.bitly,
				"longUrl":url
			}
		}
	);

	if(shortenResponse.statusCode === 200 ){
		return shortenResponse.data.data.url
	}else{
		throw new Meteor.Error(500, "Bitly call failed with errors: "+shortenResponse.status_txt);
	}

	var statsResponse = Meteor.http.get(
		"https://api-ssl.bitly.com/v3/link/clcikcs?",
		{
			timeout: 5000,
			params:{
				"format": "json",
				"access_token": Meteor.settings.bitly,
				"link": link
			}

		}
	);
	if (statsResponse.data.status_code === 200 )
		return statsResponse.data.data.link_clicks
}

Meteor.methods({
	'getBitlyClicks': function(link){
		return Bitly.getClicks(link);
	}
});

var callInterval = 10000;

Meteor.setInterval(function(){
	var shortUrlPosts = Posts.find({shortUrl: {$exists: true}});
	var postsNumber = shortUrlPosts.count();
	var count = 0;
	shortUrlPosts.forEach(function(post){
		var callTimeout = Math.round(callInterval/postsNumber*count);
		Meteor.selfTimeout(function(){
			Posts.update(post._id,{$set: {clicks: Bitly.getClicks(post.shortUrl)}});
		}, callTimeout);
		count++;


	});
},callInterval);