Meteor.publish('posts',function(options,userId){
	check(options, {
		sort: Object,
		limit: Number
	});
	var getTypes = [];
	if(this.userId){
		var user = Meteor.users.findOne(this.userId);
		var arrayed = user.profile.restrict.split(',');
		getTypes = Types.find( { $or: [{ restrictive: { $ne: true}} , { title: {$in: arrayed}}]}).fetch();
	}else
	{getTypes = Types.find({ restrictive: { $ne: true }}).fetch();}
	
	var i;
	var titleArray = [];
	for (i = 0; i < getTypes.length; i++) { 
		tempTitleId = getTypes[i]._id;
    titleArray.push(tempTitleId);
    }
   
	return Posts.find({ typeId: { $in: titleArray }}, options);
});

Meteor.publish("singlePost", function(id,user){
	check(id, String)
	var post = Posts.findOne({_id: id});
	console.log(post);
	var typist = post.typeId;
	console.log(typist);
	var category = Types.findOne({ _id: post.typeId });
	console.log(category);
	if (category.restrictive){
	
		if (this.userId){
				var usered = Meteor.users.findOne(this.userId);
			var arrayed = usered.profile.restrict.split(',');
			var found = arrayed.indexOf(category.title);
			if (found == -1)
			{	this.stop();
				return null;
			}
			else{
				return Posts.find(id);
			}
		}else{
			this.stop();
			return null;
		}

	}else
	{return Posts.find(id);}

	
});

Meteor.publish('comments',function(postId){
	check(postId, String);
	return Comments.find({postId: postId});
});

Meteor.publish('convers',function(headId){
	check(headId, String);
	return Comments.find({headId: headId});
});

Meteor.publish('ttas',function(thoughtType){
	check(thoughtType, String);
	return Comments.find({thoughtType: thoughtType});
});

Meteor.publish('notifications',function(){
	return Notifications.find({userId: this.userId, read: false});
});

Meteor.publish('types',function(){
	return Types.find();
});

Meteor.publish('postsCat',function(category){

	var typed = Types.findOne({ _id: category});
	
	if (typed.restrictive){
	
		if (this.userId){
			var user = Meteor.users.findOne( {_id: this.userId});
			var paths = user.profile.restrict;
			var arrayed = paths.split(",");
			

			var found = arrayed.indexOf(typed.title);
			console.log(found);
			if (found === -1)
			{	this.stop();
				return null;
			}
			else{
				
				return Posts.find({ typeId: category},{sort: {submitted: -1}});
			}
		}else{
			this.stop();
			return null;
		}

	}else
	{return Posts.find({ typeId: category},{sort: {submitted: -1}});}

	

});

Meteor.publish(null, function (){ 
  return Meteor.roles.find({})
});


Meteor.publish('users',function(userId){
	if (Roles.userIsInRole(this.userId,'admin')){
		return Meteor.users.find({},{});

	}else{
		this.stop();
		return;
	}
	
});